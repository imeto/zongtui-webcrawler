package com.zongtui.fourinone.park;

import java.util.EventListener;

public interface LastestListener extends EventListener
{
	public boolean happenLastest(LastestEvent le);
}